// Seleção do elemento
var documento = document.body;
var item = document.querySelector("#telaInicial");

// 1. Eventos do mouse...

// 1.1. Click
item.addEventListener("click", (evento) => evento.target.style.display = "none");

// 1.2. Doble click
item.addEventListener("dblclick", (evento) => evento.target.innerText = "Doble Click" );

documento.addEventListener('keypress', function(e) {
    var codigoTecla = e.code;
    var space = codigoTecla == 32;
    if (space) alert('O space foi pressionado!');
  });
